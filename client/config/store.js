import Vue    from 'vue'
import Vuex   from 'vuex'
import axios  from 'axios'
import config from './configServer'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        status: '',
        token:localStorage.getItem('token') || '',
        user: {}
    },
    mutations: {
        auth_request(state) {
            state.status = 'loading'
        },
        auth_success(state, token, user) {
            state.status = 'success'
            state.token = token
            state.user = user
        },
        auth_error(state) {
            state.status = 'error'
        },
        logout(state) {
            state.status = ''
            state.token = ''
        },
    },
    actions: {
        login({ commit }, user) {
            return new Promise((resolve, reject) => {
              commit('auth_request')
              
              axios({ url: config.urlServer+"/api/loginUser", data: user, method: 'POST' })
                .then(resp => {
                    if(!resp.data.error){
                        const token = resp.data.token
                        const user  = resp.data.user
                        
                        localStorage.setItem('token', token)
                        localStorage.setItem('user', JSON.stringify(user))
                        
                        // Add the following line:
                        axios.defaults.headers.common['Authorization'] = "Bearer " + token
                        commit('auth_success', token, user)

                        resolve(resp)
                    }else{
                        commit('auth_error');
                        
                        localStorage.removeItem('token');
                        localStorage.removeItem('user')
                        
                        reject(resp.data.msg);
                    }
                  
                })
                .catch(err => {
                  commit('auth_error')
                  
                  localStorage.removeItem('token')
                  localStorage.removeItem('user')
                  
                  reject(err)
                })
            })
          },
          logout({ commit }) {
            return new Promise((resolve, reject) => {
              commit('logout')
              
              localStorage.removeItem('token')
              localStorage.removeItem('user')
              
              delete axios.defaults.headers.common['Authorization']
              
              resolve()
            })
          }
    },
    getters: {
        isLoggedIn: state => !!state.token,
        authStatus: state => state.status,
    }
});