// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue           from 'vue'
import Axios         from 'axios'
import App           from './App'
import router        from './router'
import store         from '../config/store'
import Notifications from 'vue-notification'

Vue.use(Notifications)

Vue.prototype.$http = Axios
const token = localStorage.getItem('token')

if(token) {
	Vue.prototype.$http.defaults.headers.common['Authorization'] = "Bearer " + token
}

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
	el: '#app',
	store,
	router,
	components: { App },
	template: '<App/>'
})