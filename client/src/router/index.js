import Vue     from 'vue'
import Router  from 'vue-router'
import Store   from '../../config/store'
import Home    from '@/components/Home'
import Login   from '@/components/Login'

Vue.use(Router)

const router = new Router({
    mode: 'history' ,
    routes: [
        {
            path: '*',
            redirect: '/'
        },
        {
            path: '/',
            name: 'Login',
            component: Login
        },
        {
            path: '/home',
            name: 'Inicio',
            component: Home,
            meta: {
                requiresAuth: true
            }
        }
    ]
})

// Recorre cada ruta y valida si esa ruta está protegida(requireAuth: true)
router.beforeEach((to, from, next) => {
    // Validamos que la ruta tenga el metadato requiresAuth
    if(to.matched.some(record => record.meta.requiresAuth)) {
        // Validamos si se encuentra logueado
        if(Store.getters.isLoggedIn) {
            next()
            return
        }

        // Como no está logueado, me envia a la raiz del proyecto para iniciar sesión
        next('/')
    } else {
        if(Store.getters.isLoggedIn) {
            // Si está logueado y va a la raiz del proyecto, se envia al Home
            if(to.path == '/') {
                next('/home')
                return
            }
        }
        
        // Como no estoy autenticado, me envia a la vista principal
        next()
    }
})

export default router