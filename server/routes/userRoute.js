const userController = require('../controllers/userController')
const auth           = require('../middleware/auth')

router.post('/createUser', userController.createUser)
router.post('/loginUser',  userController.loginUser)
router.put('/updateUser',  auth.isAuth, userController.updateUser)