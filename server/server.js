const express    = require('express')
const bodyParser = require('body-parser')
const cors       = require('cors')

const app = express()

global.router = express.Router()

require('require-all')(__dirname + '/routes')

app.set('port', process.env.PORT || 3000)

app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())
app.use(cors())

// Rutas
app.use('/api', router)

app.listen(app.get('port'), () => {
  console.log(`Conectado al servidor correctamente, corriendo enel puerto http://localhost: ${app.get('port')}`)
})