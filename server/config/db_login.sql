CREATE DATABASE db_login;

USE db_login;

CREATE TABLE users (
    id          VARCHAR(50) NOT NULL,
    displayName VARCHAR(25) NOT NULL UNIQUE,
    email       VARCHAR(30) NOT NULL UNIQUE,
    pass        VARCHAR(65) NOT NULL,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;