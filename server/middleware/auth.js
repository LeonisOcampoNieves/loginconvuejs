require('dotenv').config()

const jwt = require('jsonwebtoken')

exports.isAuth = function(req, res, next) {
    const token = req.headers['Authorization']

    if(!token) {
        return res.status(401).send({message: 'Sin token proporcionado'})
    }

    const decode = jwt.verify(token, process.env.SECRET_TOKEN)

    req.idUser = decode.idUser

    // Pasa a la siguiente función de la ruta
    next()
}