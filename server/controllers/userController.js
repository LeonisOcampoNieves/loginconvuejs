const userModel = require('../models/userModel')
const validate  = require('../validators/userVal')
const jwt       = require('jsonwebtoken')

exports.getUserById = async function(req, res) {
    let id = req.params.id

    let user = await userModel.getUserById(id)

    res.json(user)
}

exports.createUser = async function(req, res) {
    let respuesta = await validate.validateCreateUser(req.body)

    if(respuesta.error) return res.json(respuesta)

    await userModel.createUser(req.body)

    res.json(respuesta)
}

exports.updateUser = async function(req, res) {
    let id = req.params.id
    
    let respuesta = await validate.validateUpdateUser(id, req.body)

    if(respuesta.error) return res.json(respuesta)

    await userModel.updateUser(id, req.body)

    res.json(respuesta)
}

exports.loginUser = async function(req, res) {
    let respuesta = await validate.validateLoginUser(req.body)

    if(respuesta.error) return res.json(respuesta)

    let user = respuesta.user

    // Creo el token
    /*
        Sign permite registrar o crear un token, se recibe 2 parametros
        el primero es un id de usuario(Payload), el segundo parametro
        es la clave secreta.
    */
    const token = jwt.sign({id: user.id}, process.env.SECRET_TOKEN, {
        // tiempo de expiración del token: 1 día - 60s * 60(1 hora) * 24 horas
        expiresIn : 60 * 60 * 24
    })

    respuesta = {
        error: false,
        user: {
            idUser : user.id,
            name   : user.displayName
        },
        token
    }

    res.json(respuesta)
}